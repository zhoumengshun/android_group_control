﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adb群控.util
{
    class Util
    {

        /// <summary>
        /// 字符串转hex
        /// </summary>
        /// <param name="hexString">hex字符串</param>
        /// <returns>hex Byte数组</returns>
        public  byte[] strToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }


        public  String hexToString(byte[] data)
        {
            var hex = BitConverter.ToString(data, 0).Replace("-", string.Empty).ToLower();
            return hex.ToString();
        }

        /// <summary>
        /// 地位游戏潘旭
        /// </summary>
        /// <param name="src"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public  long bytesToLong(byte[] src, int offset)
        {
            long value;
            value = (src[offset] & 0xFF) | ((src[offset + 1] & 0xFF) << 8) | ((src[offset + 2] & 0xFF) << 16)
                    | ((src[offset + 3] & 0xFF) << 24);
            return value;
        }

        public  byte[] addBytes(byte[] data1, byte[] data2)
        {
            byte[] data3 = new byte[data1.Length + data2.Length];
            Array.Copy(data1, 0, data3, 0, data1.Length);
            Array.Copy(data2, 0, data3, data1.Length, data2.Length);
            return data3;

        }

        public   long getLongLenByString(String lenData)
        {
            //先转换成long
            byte[] lenByte = strToHexByte(lenData);
            return bytesToLong(lenByte,0);
        }


        public long getNowTime()
        {
            long currentTicks = DateTime.Now.Ticks;
            DateTime dtFrom = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return (currentTicks - dtFrom.Ticks) / 10000;
        }
    }
}
